android-platform-tools-apksig (33.0.0-1) unstable; urgency=medium

  * Team upload

  [ 殷啟聰 | Kai-Chung Yan ]
  * Remove myself from Uploaders

  [ Sunday Nkwuda ]
  * New upstream version 33.0.0
  * refresh and update patches
  * add libbcprov-java to Build-depends
  * Declare compliance with Debian Policy 4.6.1
  * Update copyright

 -- Sunday Nkwuda <sonnietech@disroot.org>  Sun, 18 Sep 2022 22:51:31 +0000

android-platform-tools-apksig (31.0.2-1) unstable; urgency=medium

  * Team upload
  * New upstream version 31.0.2
  * patches refreshed
  * patched to remove Conscrypt
  * fixed Homepage:

 -- Manas Kashyap <manaskashyaptech@gmail.com>  Tue, 14 Dec 2021 11:55:52 +0100

android-platform-tools-apksig (30.0.3-4) unstable; urgency=medium

  * Team upload
  * team package, remove myself from Uploaders:
  * install all required help text files (Closes: #985800)
  * add autopkgtest tests for all help commands
  * add rotate and lineage to man page

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 23 Mar 2021 22:43:56 +0100

android-platform-tools-apksig (30.0.3-3) unstable; urgency=medium

  * add Homepage: to debian/upstream/metadata
  * gitlab-ci: remove redundant bits
  * fix man page formatting (Closes: #960778)

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 24 Nov 2020 20:55:58 +0100

android-platform-tools-apksig (30.0.3-2) unstable; urgency=medium

  * Use upstream's apksigner script rather than binfmt_misc (Closes: #963877)

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 20 Nov 2020 17:24:52 +0100

android-platform-tools-apksig (30.0.3-1) unstable; urgency=medium

  * Team upload

  [ Hans-Christoph Steiner ]
  * New upstream version 30.0.3

  [ Manas Kashyap ]
  * watch file modified accordingly to check for upstream resources
  * debian uupdated removed, no longer needed
  * debhelper bumped to version 13
  * README.source modified, adding the apk entry found under test
  * maven.rules added for junit, copied from gradle

 -- Manas Kashyap <manaskashyaptech@gmail.com>  Sat, 11 Jul 2020 01:06:24 +0530

android-platform-tools-apksig (0.9-2) unstable; urgency=medium

  * Team Upload
  * Corrects en-dashes in manpage  (Closes: #960778)
  * Bumps standard version to 4.5.0 (No changes Required)
  * Use of debhelper-compat (removes d/compat)

 -- Samyak Jain <samyak.jn11@gmail.com>  Sat, 06 Jun 2020 11:09:07 +0530

android-platform-tools-apksig (0.9-1) unstable; urgency=medium

  * New upstream version from tag android-10.0.0_r20
  * Rules-Requires-Root: no
  * update patch for new upstream version
  * Standards-Version: 4.4.1 no changes
  * fix upstream-metadata-file-is-missing

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 27 Dec 2019 20:58:23 +0100

android-platform-tools-apksig (0.8-3) unstable; urgency=medium

  * Compile for Java 8: (Closes: #925320, LP: #1821235)
    - debian/rules: the parameter '-target 8' must be set to get got compiled
      for OpenJDK 8, setting '--release 8' is the best way as it guarantees
      that the whole '-source/-target/-bootclasspath' triad is correctly set.

 -- Tiago Stürmer Daitx <tiago.daitx@ubuntu.com>  Fri, 22 Mar 2019 21:15:35 +0000

android-platform-tools-apksig (0.8-2) unstable; urgency=medium

  [ Emmanuel Bourg ]
  * Removed the alternative build dependency on openjdk-8-jdk

  [ Kai-Chung Yan ]
  * Install Maven artifacts of `apksig.jar`
  * Standards-Version => 4.2.1
  * uscan: Watch for `gradle-*` tags instead of `android-*_r*`
  * Instal Maven artifacts for `apkgsig.jar`
  * Remove unused build-dependencies: antlr3 groovy maven-debian-helper
  * apksigner now requires JRE >= 8 since it is compiled on source level 8
  * Add myself to Uploaders
  * Install `apksigner` into Android SDK home

  [ Hans-Christoph Steiner ]
  * Use real URLs for Vcs-Git, not ones that redirect
  * Add autopkgtest for APK verification
  * Add GitLab CI support

 -- Kai-Chung Yan <seamlikok@gmail.com>  Fri, 19 Oct 2018 17:37:17 +0800

android-platform-tools-apksig (0.8-1) unstable; urgency=medium

  * New upstream release from tag android-8.1.0_r14

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 21 Feb 2018 21:28:29 +0100

android-platform-tools-apksig (0.5+git168~g10c9d71-1) unstable; urgency=medium

  * New upstream release (Closes: #859541)

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 04 Apr 2017 21:25:56 +0200

android-platform-tools-apksig (0.5+git165~g42d07eb-1) unstable; urgency=medium

  * New upstream release (Closes: #857027)
  * Add bash-completion

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 10 Mar 2017 13:58:11 +0100

android-platform-tools-apksig (0.4+git162~g85a854b-1) unstable; urgency=medium

  * New upstream release

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 23 Dec 2016 09:01:19 +0000

android-platform-tools-apksig (0.3+git154~gfdc6e98-1) unstable; urgency=medium

  * Initial release. (Closes: #847657)

 -- Hans-Christoph Steiner <hans@eds.org>  Sat, 10 Dec 2016 12:48:20 +0000
